package com.xm.lib.common.base.rv.v1

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * RecyclerView 适配器基类
 */
@Deprecated("")
abstract class BaseRvAdapter : RecyclerView.Adapter<BaseViewHolder>() {
    open var data: ArrayList<Any>? = ArrayList()
    private var delegateManager: DelegateManager? = null
    @Deprecated("不要在使用")
    private var onBindDataListener: OnBindDataListener? = null

    init {
        delegateManager = DelegateManager()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(getItemViewLayoutId(viewType), parent, false)
        val viewHolder: BaseViewHolder? = getViewHolderClass(viewType).getConstructor(View::class.java).newInstance(v) as BaseViewHolder?
        delegateManager?.typeViewHolders?.add(viewHolder!!)
        return viewHolder!!
    }

    override fun getItemCount(): Int {
        return if (data?.isEmpty()!!) 0 else data?.size!!
    }

    override fun getItemViewType(position: Int): Int {
        for (d in delegateManager?.targets?.entries!!) {
            if (d.value.beanClass == data!![position].javaClass) {
                return d.key
            }
        }
        return super.getItemViewType(position)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        bindVH(holder, position)
    }

    private fun getDelegateBean(viewType: Int): DelegateBean? {
        return delegateManager?.targets?.get(viewType)!!
    }

    private fun getItemViewLayoutId(viewType: Int): Int {
        return getDelegateBean(viewType)!!.itemViewLayoutId!!
    }

    private fun getViewHolderClass(viewType: Int): Class<*> {
        return getDelegateBean(viewType)!!.viewHolderClass
    }

    private fun bindVH(holder: BaseViewHolder, position: Int) {
        if (data?.get(position) != null) {
            holder.bindData(data!![position], position)
            holder.bindData(this, data!![position], position)
            onBindDataListener?.onBindData(holder.itemView, data!![position], position)
        }
    }

    /**
     * 委派者添加需要被委派任务的ViewHolder
     */
    fun addItemViewDelegate(viewType: Int, viewHolderClass: Class<*>, BeanClass: Class<*>, itemViewLayoutId: Int?) {
        delegateManager?.targets?.put(viewType, DelegateBean(viewHolderClass, BeanClass, itemViewLayoutId))
    }

    fun getTypeViewHolder(type: Int): BaseViewHolder? {
        var vh: BaseViewHolder? = null
        try {
            vh = delegateManager?.typeViewHolders?.get(type)!!
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return vh
    }

    private class DelegateManager {
        val targets = HashMap<Int, DelegateBean>()
        val typeViewHolders = ArrayList<BaseViewHolder>()
    }

    /**
     * viewHolderClass   RecyclerView的VH
     * beanClass         VH 对应的实体
     * itemViewLayoutId  VH 创建的实体类
     */
    private class DelegateBean(val viewHolderClass: Class<*>, val beanClass: Class<*>, val itemViewLayoutId: Int?)
}

