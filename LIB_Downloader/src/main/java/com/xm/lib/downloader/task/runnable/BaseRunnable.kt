package com.xm.lib.downloader.task.runnable



import com.xm.lib.common.log.BKLog
import com.xm.lib.downloader.DownManager
import com.xm.lib.downloader.config.DownConfig.Companion.DEFAULT
import com.xm.lib.downloader.config.DownConfig.Companion.EMPTY_STRING
import com.xm.lib.downloader.enum_.DownErrorType
import java.io.RandomAccessFile
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.atomic.AtomicBoolean

abstract class BaseRunnable : Runnable {
    /**
     * 下载线程是否运行中
     */
    var runing = AtomicBoolean(false) //运行状态
    var threadName = EMPTY_STRING  //线程名称
    var process: Long = DEFAULT.toLong() // 下载进度（单位 B）
    var total: Long = DEFAULT.toLong() //下载文件总大小（单位 B）
    var present: Float = DEFAULT.toFloat() //下载进度(单位 %)
    var listener: BaseRunnable.OnListener? = null //下载监听
    var downManager: DownManager?=null // 下载管理类

    /**
     * 多线程会使用到的属性
     */
    var url = EMPTY_STRING  //下载地址
    var name = EMPTY_STRING //下载名称
    var path = EMPTY_STRING //下载根目录
    var dir = EMPTY_STRING //下载目录

    /**
     * 单线程会使用到的属性
     */
    var raf: RandomAccessFile? = null //下载文件文件定位到之前下载的位置

    override fun run() {
        runing(true)
        down()
    }

    abstract fun down()

    abstract fun exit()

    fun iniConn(): HttpURLConnection {
        val conn = URL(url).openConnection() as HttpURLConnection
        conn.requestMethod = "GET"
        conn.connectTimeout = 15000
        conn.readTimeout = 15000
        conn.doInput = true
        conn.setRequestProperty("Accept-Encoding", "identity")
        return conn
    }

    fun runing(flag: Boolean) {
        runing.set(flag)
        if (!flag) {
            //BKLog.d("BaseRunnable", "$threadName 停止下载线程")
        }
    }

    /**
     * 下载监听
     */
    interface OnListener {
        /**
         * 下载进度
         */
        fun onProcess(baseRunnable: BaseRunnable, process: Long, total: Long, present: Float)

        /**
         * 下载完成
         */
        fun onComplete(baseRunnable: BaseRunnable, total: Long)

        /**
         * 下载错误
         */
        fun onError(baseRunnable: BaseRunnable, type: DownErrorType, s: String)
    }
}