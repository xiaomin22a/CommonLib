package com.xm.lib.downloader.task.runnable

import com.xm.lib.common.log.BKLog
import com.xm.lib.downloader.DownManager
import com.xm.lib.downloader.config.DownConfig.Companion.DEFAULT
import com.xm.lib.downloader.enum_.DownErrorType
import com.xm.lib.downloader.task.DownTask
import com.xm.lib.downloader.utils.FileUtil
import com.xm.lib.downloader.utils.FileUtil.getSizeUnit
import com.xm.lib.downloader.utils.FileUtil.getUsableSpace
import java.io.BufferedInputStream
import java.io.InputStream
import java.io.RandomAccessFile
import java.net.HttpURLConnection

/**
 *
 * 断点下载知识点
 * (1) 请求头Range,格式 Range:bytes=start-end
 *     例子:
 *     range:bytes=10-     代表请求文件第10个至文件最后字节的数据
 *     range:bytes=40-100  代表请求文件第10个至第100个字节之间的的数据
 *
 * (2) 服务器支持返回206状态,反之返回200.
 *
 * (3）知识链接,RandomAccessFile的使用
 *     https://www.cnblogs.com/1995hxt/p/5692050.html
 */
open class SingleRunnable2 : BaseRunnable() {

    companion object {
        const val TAG = "SingleRunnable"
        var DEFAULT_BUFFER_SIZE = 0

        /**
         * @param task 下载任务的信息包括文件名称、链接、下载进度等
         * @param downManager 下载管理类包括下载线程创建、数据库创建、下载分发器、观察者。
         * @param listener 下载监听进度、完成、错误
         */
        fun newSingleRunnable2(task: DownTask?, downManager: DownManager?, listener: BaseRunnable.OnListener?): SingleRunnable2? {
            val singleRunnable = SingleRunnable2()
            if (task == null || downManager == null || listener == null) {
                BKLog.e(TAG, "newSingleRunnable2 is failure")
                return singleRunnable
            }
            // 下载缓存文件定位到之前已经下载的位置
            val (startIndex, raf) = downCacheFileSeek(task)

            // 设置读写buf单位
            setWriteFileBuf(downManager)

            // 下载信息设置
            singleRunnable.name = task.name
            singleRunnable.url = task.url
            singleRunnable.total = task.total
            singleRunnable.present = task.present.toFloat()
            singleRunnable.process = startIndex
            singleRunnable.downManager = downManager
            singleRunnable.threadName = "SingleRunnable2"
            singleRunnable.raf = raf
            singleRunnable.rangeStartIndex = startIndex
            singleRunnable.rangeEndIndex = -1
            singleRunnable.listener = listener

            // BKLog.d(TAG, "downCacheFileSeek : $startIndex  writeFileBuf : $DEFAULT_BUFFER_SIZE")
            return singleRunnable
        }

        private fun setWriteFileBuf(downManager: DownManager) {
            DEFAULT_BUFFER_SIZE = downManager.downConfig()!!.bufferSize
        }

        private fun downCacheFileSeek(task: DownTask): Pair<Long, RandomAccessFile> {
            val file = FileUtil.createNewFile(task.path, task.dir, task.fileName)
            //val startIndex = file.length()
            val startIndex = 0L
            val raf = RandomAccessFile(file, "rwd")
            raf.seek(startIndex)
            return Pair(startIndex, raf)
        }
    }

    /**
     * conn对象
     */
    private var conn: HttpURLConnection? = null
    /**
     * 是否完成
     */
    var isComplete = false
    /**
     * 设置读写buf单位
     */
    private var bufferSize = DEFAULT_BUFFER_SIZE
    /**
     * 请求头Range:bytes=start-end 的start值
     */
    var rangeStartIndex = DEFAULT
    /**
     * 请求头Range:bytes=start-end 的end值,一般设置为-1
     */
    var rangeEndIndex = DEFAULT

    override fun down() {
        try {
            if (isRunning() == false) {
                return
            }
            // 设置请求头Range这是断点下载的关键
            setHttpRangeHead()
            if (!isEnoughSpace()) {
                return
            }
            val inputStream: InputStream? = getConn().inputStream
            if (!isEnoughSpace()) {
                return
            }

            // BKLog.d(TAG, "code:${getConn().responseCode}")
            write(inputStream, raf)
            //callBackComplete()  //完成回调
        } catch (e: Exception) {
            e.printStackTrace()
            BKLog.e(TAG,"down is failure,$e")
            callBackError(DownErrorType.UNKNOWN, e.toString())
        }
    }

    private fun isEnoughSpace(): Boolean {
        total = getConn().contentLength.toLong() + rangeStartIndex
        if (total > getUsableSpace()) {
            BKLog.e(TAG,"not EnoughSpace,down failure")
            callBackError(DownErrorType.NO_SPACE, "空间不足，下载资源大小 ：${getSizeUnit(total)} 可用资源大小 ：${getSizeUnit(getUsableSpace())}")
            return false
        }
        return true
    }

    private fun isRunning(): Boolean? {
        return downManager?.runFlag;
    }

    private fun setHttpRangeHead() {
        getConn().setRequestProperty("Range", getRangeHeadValue())
    }

    private fun getConn(): HttpURLConnection {
        if (conn == null) {
            conn = iniConn()
        }
        return conn!!
    }

    private fun getRangeHeadValue(): String {
        val value = if (/*rangeStartIndex > 0 && */rangeEndIndex > 0) {
            //BKLog.d(TAG, "请求头 bytes=$rangeStartIndex-$rangeEndIndex")
            "bytes=$rangeStartIndex-$rangeEndIndex"
        } else {
            //BKLog.d(TAG, "请求头 bytes=$rangeStartIndex-")
            "bytes=$rangeStartIndex-"
        }
        return value
    }

    private fun write(inputStream: InputStream?, raf: RandomAccessFile?) {
        if (inputStream == null || raf == null) {
            BKLog.e(TAG, "write is failure , inputStream or raf is null")
            callBackError(DownErrorType.UNKNOWN, "")
            return
        }

        var length: Int
        val bis = BufferedInputStream(inputStream)
        val buffer = ByteArray(bufferSize)
        try {
            while (true) {
                length = bis.read(buffer)

                if (writeComplete(length)) return
                if (stopWrite()) return

                // 进度回调
                callBackProcess(length.toLong())
                // 写入文件中
                raf.write(buffer, 0, length)
            }
        } catch (e: Exception) {
            BKLog.e(TAG, "write exception, callBack down error")
            callBackError(DownErrorType.UNKNOWN, e.toString())
        } finally {
            raf.close()
            bis.close()
        }
    }

    private fun writeComplete(length: Int): Boolean {
        if (length == -1) {
            // BKLog.d(TAG, "writeComplete")
            isComplete = true
            callBackComplete()
            return true
        }
        return false
    }

    private fun stopWrite(): Boolean {
        if (!runing.get()) {
            BKLog.d(TAG, "stopWrite by runing flag")
            return true
        }
        // todo m3u8下载解析地址需要一个全局标志位
        if (downManager?.runFlag == false) {
            BKLog.d(TAG, "stopWrite by runFlag")
            return true
        }
        return false
    }

    private fun callBackProcess(length: Long) {

        if (listener == null) {
            BKLog.e(TAG,"listener is null, callBackProcess failure")
            return
        }
        this.process += length
        val present = (100 * this.process / total).toFloat()
        listener!!.onProcess(this, process, total, present)
        runing.set(true)
    }

    private fun callBackComplete() {
        if (listener == null) {
            BKLog.e(TAG,"listener is null, callBackComplete failure")
            return
        }
        //ps:在使用MultiRunnable下载时process total是不对应的，所以在这里加了标志位。
        if (runing.get() && isComplete) {
            listener!!.onComplete(this, total)
            runing.set(false)
        }
    }

    private fun callBackError(type: DownErrorType, errorStr: String) {
        if (listener == null) {
            BKLog.e(TAG,"listener is null, callBackError failure")
            return
        }
        listener!!.onError(this, type, errorStr)
        runing.set(false)
    }

    override fun exit() {
        runing(false)
    }
}
