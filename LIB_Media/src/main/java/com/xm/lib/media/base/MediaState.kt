package com.xm.lib.media.base

/**
 * 播放状态
 */
enum class MediaState {
    MEDIA_STATE_ERROR,
    MEDIA_STATE_PREPARED,
    MEDIA_STATE_COMPLETION
}